﻿using HorseInCoatApplication.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HorseInCoatApplication.NinjectHelper
{
    class DailyChatModule : Ninject.Modules.NinjectModule
    {
        public override void Load()
        {
            Bind<IHorseInCoatDbContext>().To<ApplicationDbContext>();
        }
    }
}