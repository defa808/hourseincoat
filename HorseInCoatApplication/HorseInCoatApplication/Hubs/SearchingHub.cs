﻿using DailyChatApiV5.NinjectHelper;
using HorseInCoatApplication.Models;
using HorseInCoatApplication.Models.GameFolder;
using HorseInCoatApplication.Services;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.SignalR;
using Ninject;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace HorseInCoatApplication.Hubs
{
    public class SearchingHub : Hub
    {
        static Dictionary<string, string> userConnectionDictionary = new Dictionary<string, string>();

        public void Search()
        {
            using (IHorseInCoatDbContext db = NjectHelper.Kernel.Get<IHorseInCoatDbContext>())
            {
                string user1Id = Context.User.Identity.GetUserId();

                if (!userConnectionDictionary.ContainsKey(user1Id))
                {
                    base.OnDisconnected(false);
                }

                SearchingService searchingService = new SearchingService(userConnectionDictionary.Keys.ToList());

                string user2Id = searchingService.GetUserFor(user1Id);

                if (user2Id == null)
                    return;

                ApplicationUser user1 = db.GetUsers().First(x => x.Id == user1Id);
                ApplicationUser user2 = db.GetUsers().First(x => x.Id == user2Id);

                Game game = new Game()
                {
                    TypeGame = TypeGame.PlayerPlayer,
                    
                };

                GameStatistic gameStatic1 = new GameStatistic()
                {
                    Game = game,
                    Data = DateTime.Now,
                    ResultGame = null,
                    UserId = user1Id
                };
                GameStatistic gameStatic2 = new GameStatistic()
                {
                    Game = game,
                    Data = DateTime.Now,
                    ResultGame = null,
                    UserId = user2Id
                };


                db.Statistics.Add(gameStatic1);
                db.Statistics.Add(gameStatic2);
                db.Games.Add(game);

                db.SaveChanges();




            }
        }

        public override Task OnConnected()
        {
            string userId = Context.User.Identity.GetUserId();

            if (userConnectionDictionary.ContainsKey(userId))
                userConnectionDictionary[userId] = Context.ConnectionId;
            else
                userConnectionDictionary.Add(userId, Context.ConnectionId);

            return base.OnConnected();
        }

        public override Task OnReconnected()
        {

            string userId = Context.User.Identity.GetUserId();

            if (userConnectionDictionary.ContainsKey(userId))
                userConnectionDictionary[userId] = Context.ConnectionId;
            else
                userConnectionDictionary.Add(userId, Context.ConnectionId);


            return base.OnReconnected();
        }

        public override Task OnDisconnected(bool stopCalled)
        {
            string userId = Context.User.Identity.GetUserId();

            Clients.Client(Context.ConnectionId).serverOrderedDisconnect();

            if (userConnectionDictionary.ContainsValue(Context.ConnectionId))
                userConnectionDictionary.Remove(userId);

            return base.OnDisconnected(stopCalled);


        }

    }
}