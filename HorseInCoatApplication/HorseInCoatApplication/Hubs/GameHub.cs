﻿using DailyChatApiV5.NinjectHelper;
using HorseInCoatApplication.Models;
using HorseInCoatApplication.Models.GameFolder;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.SignalR;
using Ninject;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace HorseInCoatApplication.Hubs
{
    public class GameHub : Hub
    {
        //keys user values connectionid
        static Dictionary<string, string> userConnectionDictionary = new Dictionary<string, string>();

        public void DoStep(int coordinateX, int coordinateY, int gameId)
        {
            string userId = Context.User.Identity.GetUserId();

            using (IHorseInCoatDbContext db = NjectHelper.Kernel.Get<IHorseInCoatDbContext>())
            {

                Step step = new Step()
                {
                    GameId = gameId,
                    CoordinateX = coordinateX,
                    CoordinateY = coordinateY
                };

                db.Steps.Add(step);

                Game game = db.Games.First(x => x.Id == gameId);

                string user2 = game.GameStatistic.Where(x => x.UserId != userId).Select(x => x.UserId).First();

                IEnumerable<PointViewModel> closedSteps = db.Steps.Where(x => x.GameId == gameId).Select(x => new PointViewModel() { coordinateX = x.CoordinateX, coordinateY = x.CoordinateY });
                IEnumerable<PointViewModel> possibleSteps = GetPossibleSteps(gameId, coordinateX, coordinateY, closedSteps);

                if (userConnectionDictionary.TryGetValue(user2.ToString(), out string userConnectId))
                    Clients.Client(userConnectId).renderStep(coordinateX, coordinateY, possibleSteps);

            }


        }

        private IEnumerable<PointViewModel> GetPossibleSteps(int gameId, int coordinateX, int coordinateY, IEnumerable<PointViewModel> closedSteps)
        {
            List<PointViewModel> possibleSteps = new List<PointViewModel>();

            for (int firstStep = 1; firstStep <= 3; firstStep++)
            {
                int secondStep = 3 % firstStep;
                //Upper situation
                if (coordinateY - secondStep > 0)
                {
                    //rightWay
                    if (coordinateX + secondStep < 8)
                        possibleSteps.Add(new PointViewModel() { coordinateX = coordinateX + secondStep, coordinateY = coordinateY - firstStep });

                    //leftWay
                    if (coordinateX - secondStep > 0)
                        possibleSteps.Add(new PointViewModel() { coordinateX = coordinateX - secondStep, coordinateY = coordinateY - firstStep });

                }
                //Under situation
                if (coordinateY + secondStep < 8)
                {
                    //rightWay
                    if (coordinateX + secondStep < 8)
                        possibleSteps.Add(new PointViewModel() { coordinateX = coordinateX + secondStep, coordinateY = coordinateY - firstStep });

                    //leftWay
                    if (coordinateX - secondStep > 0)
                        possibleSteps.Add(new PointViewModel() { coordinateX = coordinateX + secondStep, coordinateY = coordinateY - firstStep });
                }
            }

            return possibleSteps.Except(closedSteps);
        }


        public override Task OnConnected()
        {
            string userId = Context.User.Identity.GetUserId();

            if (userConnectionDictionary.ContainsKey(userId))
                userConnectionDictionary[userId] = Context.ConnectionId;
            else
                userConnectionDictionary.Add(userId, Context.ConnectionId);

            return base.OnConnected();
        }

        public override Task OnReconnected()
        {
            string userId = Context.User.Identity.GetUserId();

            if (userConnectionDictionary.ContainsKey(userId))
                userConnectionDictionary[userId] = Context.ConnectionId;
            else
                userConnectionDictionary.Add(userId, Context.ConnectionId);


            return base.OnReconnected();
        }

        public override Task OnDisconnected(bool stopCalled)
        {
            string userId = Context.User.Identity.GetUserId();

            Clients.Client(Context.ConnectionId).serverOrderedDisconnect();

            if (userConnectionDictionary.ContainsValue(Context.ConnectionId))
                userConnectionDictionary.Remove(userId);

            return base.OnDisconnected(stopCalled);


        }

     
    }
}