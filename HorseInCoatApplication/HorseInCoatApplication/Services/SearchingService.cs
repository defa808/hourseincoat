﻿using DailyChatApiV5.NinjectHelper;
using HorseInCoatApplication.Models;
using Ninject;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HorseInCoatApplication.Services
{
    public class SearchingService
    {
        List<string> Users;

        public SearchingService(List<string> userIds)
        {
            Users = userIds;

        }

        public string GetUserFor(string userId)
        {
            using (IHorseInCoatDbContext db = NjectHelper.Kernel.Get<IHorseInCoatDbContext>())
            {
                Random random = new Random();

                IQueryable<ApplicationUser> searchingUsersQuery = ((ApplicationDbContext)db).Users.Where(x =>
                                                  x.Id != userId
                                                  && Users.Contains(x.Id)
                                                && x.GameStatistics.All(y => y.ResultGame != null)
                                               );

                int countMax = searchingUsersQuery.Count();
                int rand = random.Next(0, countMax);

                ApplicationUser user2 = searchingUsersQuery.Skip(rand).FirstOrDefault();
                if (user2 == null)
                    return null;

                return user2.Id;
            }
        }
    }
}