﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(HorseInCoatApplication.Startup))]
namespace HorseInCoatApplication
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
            ConfigureSignalR(app);
        }
    }
}
