﻿using DailyChatApiV5.NinjectHelper;
using HorseInCoatApplication.Models;
using HorseInCoatApplication.Models.GameFolder;
using Microsoft.AspNet.Identity;
using Ninject;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace HorseInCoatApplication.Controllers
{
    public class ConfigurationController : Controller
    {
        // GET: Statistic
        public void SaveConfiguration(string hourseColor)
        {
            using (IHorseInCoatDbContext db = NjectHelper.Kernel.Get<IHorseInCoatDbContext>())
            {
                string userId = User.Identity.GetUserId();
                Configuration configuration = new Configuration()
                {
                    UserId = userId,
                    HorseColor = hourseColor,
                };

                db.Configurations.Add(configuration);
                db.SaveChanges();
            }




        }
    }
}