﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Microsoft.AspNet.SignalR;
using Owin;

namespace HorseInCoatApplication
{
	public partial class Startup
	{

        public void ConfigureSignalR(IAppBuilder app)
        {
            app.MapSignalR("/signalr", new HubConfiguration());

            GlobalHost.Configuration.ConnectionTimeout = TimeSpan.FromSeconds(5);
            // Wait a maximum of 30 seconds after a transport connection is lost
            // before raising the Disconnected event to terminate the SignalR connection.
            GlobalHost.Configuration.DisconnectTimeout = TimeSpan.FromSeconds(6);
            // For transports other than long polling, send a keepalive packet every
            // 10 seconds. 
            // This value must be no more than 1/3 of the DisconnectTimeout value.
            GlobalHost.Configuration.KeepAlive = TimeSpan.FromSeconds(2);
            //Setting up the message buffer size
            GlobalHost.Configuration.DefaultMessageBufferSize = 500;
        }
    }

}