﻿using HorseInCoatApplication.Models.GameFolder;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace HorseInCoatApplication.Models
{
    public interface IHorseInCoatDbContext :IDisposable
    {
         DbSet<Step> Steps { get; set; }
         DbSet<Game> Games { get; set; }
         DbSet<Configuration> Configurations { get; set; }
         DbSet<GameStatistic> Statistics { get; set; }

        IDbSet<ApplicationUser> GetUsers();
        int SaveChanges();

    }
}