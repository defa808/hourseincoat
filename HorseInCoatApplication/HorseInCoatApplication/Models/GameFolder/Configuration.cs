﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HorseInCoatApplication.Models.GameFolder
{
    public class Configuration
    {
        public int Id { get; set; }
        public string UserId { get; set; }
        public ApplicationUser User { get; set; }
        public string HorseColor { get; set; }
    }
}