﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HorseInCoatApplication.Models.GameFolder
{
    public class Game
    {
        public int Id { get; set; }
        public TypeGame TypeGame { get; set; }
        public virtual List<GameStatistic> GameStatistic { get; set; }
        public virtual List<Step> Steps { get; set; }

        public Game()
        {
            GameStatistic = new List<GameStatistic>();
            Steps = new List<Step>();
        }
    }
}