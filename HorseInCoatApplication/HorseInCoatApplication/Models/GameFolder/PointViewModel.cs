﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HorseInCoatApplication.Models.GameFolder
{
    public class PointViewModel
    {
        public int coordinateX { get; set; }
        public int coordinateY { get; set; }
    }
}