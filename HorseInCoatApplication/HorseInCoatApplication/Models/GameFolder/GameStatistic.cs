﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HorseInCoatApplication.Models.GameFolder
{
    public class GameStatistic
    {
        public int Id { get; set; }
        public string UserId { get; set; }
        public virtual ApplicationUser User { get; set; }
        public int GameId { get; set; }
        public virtual Game Game { get; set; }
        public bool? ResultGame { get; set; }
        public DateTime Data { get; set; }
    }
}