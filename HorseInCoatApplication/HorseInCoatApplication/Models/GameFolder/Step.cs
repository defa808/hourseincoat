﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HorseInCoatApplication.Models.GameFolder
{
    public class Step
    {
        public int Id { get; set; }
        public TypeStep TypeStep { get; set; }
        public int GameId { get; set; }
        public Game Game { get; set; }
        public int CoordinateX { get; set; }
        public int CoordinateY { get; set; }
    }
}