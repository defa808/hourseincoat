﻿using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using HorseInCoatApplication.Models.GameFolder;

namespace HorseInCoatApplication.Models
{
    public class ApplicationDbContext : IdentityDbContext<ApplicationUser>, IHorseInCoatDbContext
    {

        public DbSet<Step> Steps { get; set; }
        public DbSet<Game> Games { get; set; }
        public DbSet<Configuration> Configurations { get; set; }
        public DbSet<GameStatistic> Statistics { get; set; }

        public ApplicationDbContext()
            : base("DefaultConnection", throwIfV1Schema: false)
        {
        }

        public static ApplicationDbContext Create()
        {
            return new ApplicationDbContext();
        }

        public IDbSet<ApplicationUser> GetUsers()
        {
            return Users;
        }
    }
}
